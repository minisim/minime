import minisim
import numpy as np

net=minisim.IafNetwork()
net.allocate(10)

n=np.arange(0,10)
w=2.0*np.ones(10)
d=np.ones(10)

net.connect_one_to_one(n,n,w,d)
