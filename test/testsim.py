import time
import minisim
import numpy as np
from matplotlib import pyplot as plt

class brunel_network:
    ne=10000
    ni=2500
    ce=1000
    ci=250
    g=5.0
    JE=0.1
    d=1.5

    def __init__(self):
        self.JI=-self.g*self.JE
        self.n_neurons=self.ne+self.ni
        self.state=0

    def create(self):
        if self.state==0:
            self.state+=1
            self.net=minisim.IafNetwork()
            self.startbuild=time.time()

            self.net.allocate(self.n_neurons)

            self.net.connect_fixed_indegree(0,self.ne,0,self.n_neurons,self.ce,self.JE,self.d)
            self.net.connect_fixed_indegree(self.ne,self.ni,0,self.n_neurons,self.ci,self.JI,self.d)
            self.endbuild=time.time()

    def run(self, simtime=1000.):
        if self.state==0:
            self.create()
        if self.state==1:
            self.state+=1
            self.simtime=simtime
            self.net.simulate(simtime)
            self.endsimulate=time.time()

            self.build_time = self.endbuild - self.startbuild
            self.sim_time = self.endsimulate - self.endbuild

            ks=self.net.get_kernel_status()
            self.num_synapses=int(self.ce * self.n_neurons)+int(self.ci * self.n_neurons)
            self.rates=np.array(self.net.get_status('rate'))
            self.rate_ex=self.rates[0:self.ne].mean()
            self.rate_in=self.rates[self.ne:self.n_neurons].mean()

    def print_summary(self):
        if self.state==2:
            print("Brunel network simulation (MiniSim)")
            print("Number of neurons : {0}".format(self.n_neurons))
            print("Number of synapses: {0}".format(self.num_synapses))
            print("       Exitatory  : {0}".format(int(self.ce * self.n_neurons)))
            print("       Inhibitory : {0}".format(int(self.ci * self.n_neurons)))
            print("Excitatory rate   : %.2f Hz" % self.rate_ex)
            print("Inhibitory rate   : %.2f Hz" % self.rate_in)
            print("Building time     : %.2f s" % self.build_time)
            print("Simulation time/s : %.2f" % (self.sim_time/self.simtime*1000.))

    def plot(self):
        if self.state==2:
            events=self.net.get_kernel_status('events')
            s_ids= np.array(events['senders'])
            s_times= np.array(events['times'])*0.1

            sel=np.where(s_ids<50)
            plt.plot(s_times[sel], s_ids[sel],".b")
            plt.show()

net=brunel_network()  # type: brunel_network
net.run()
net.print_summary()
net.plot()




