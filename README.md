This directory contains the fast simulator for integrate and fire networks.

To compile the C++ program for the Brunel network, call
```
make
```

To compile the Python extension, call
```
python setup.py build
```

Then set the PYTHONPATH:
``` 
export PYTHONPATH=<path-to-minisim>/build/<architecture>
```

You can then start python and import the module.
```
import minisim
```
