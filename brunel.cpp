#include <iostream>
//#include <omp.h>
#include "experiment.h"

int main()
{
	//omp_set_num_threads( 4 );
  Experiment net;


  net.connect();
  net.run(10000.0);
  net.save("spikes.gdf");
  return 0;
}
