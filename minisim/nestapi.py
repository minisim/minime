# -*- coding: utf-8 -*-
#
# hl_api_helper.py
#
# This file is part of NEST.
#
# Copyright (C) 2004 The NEST Initiative
#
# NEST is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 2 of the License, or
# (at your option) any later version.
#
# NEST is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with NEST.  If not, see <http://www.gnu.org/licenses/>.

"""
High-level API for CymuK
"""

import types
import collections
import cymukkernel as _kernel

# These variables MUST be set by __init__.py right after importing.
# There is no safety net, whatsoever.

engine=_kernel.IaF_Network()

def get_unistring_type():
    """Returns string type dependent on python version.

    Returns
    -------
    str or basestring:
        Depending on Python version

    """
    import sys
    if sys.version_info[0] < 3:
        return basestring
    return str

uni_str = get_unistring_type()


def is_string(obj):
    """Check whether obj is a unicode string

    Parameters
    ----------
    obj : object
        Object to check

    Returns
    -------
    bool:
        True if obj is a unicode string
    """
    return isinstance(obj, uni_str)


def is_iterable(seq):
    """Return True if the given object is an iterable, False otherwise.

    Parameters
    ----------
    seq : object
        Object to check

    Returns
    -------
    bool:
        True if object is an iterable
    """

    try:
        iter(seq)
    except TypeError:
        return False

    return True


def is_sequence(seq):
    """Checks whether a given object is coercible to a SLI array

    Parameters
    ----------
    seq : object
        Object to check

    Returns
    -------
    bool:
        True if object is coercible to a SLI array
    """

    import sys

    if sys.version_info[0] >= 3:
        return isinstance(seq, (tuple, list, range))
    else:
        return isinstance(seq, (tuple, list, xrange))


def is_sequence_of_gids(seq):
    """Checks whether the argument is a potentially valid sequence of
    GIDs (non-negative integers).

    Parameters
    ----------
    seq : object
        Object to check

    Returns
    -------
    bool:
        True if object is a potentially valid sequence of GIDs
    """

    return is_sequence(sequ) and all(isinstance(n, int) and n >= 0 for n in seq)


def broadcast(item, length, allowed_types, name="item"):
    """Broadcast item to given length.

    Parameters
    ----------
    item : object
        Object to broadcast
    length : int
        Length to broadcast to
    allowed_types : list
        List of allowed types
    name : str, optional
        Name of item

    Returns
    -------
    object:
        The original item broadcasted to sequence form of length

    Raises
    ------
    TypeError


    """

    if isinstance(item, allowed_types):
        return length * (item,)
    elif len(item) == 1:
        return length * item
    elif len(item) != length:
        raise TypeError("'{0}' must be a single value, a list with " +
                        "one element or a list with {1} elements.".format(
                            name, length))
    return item


def version():
    """Return the CymuK version.

    Returns
    -------
    str:
        The version of CymuK.
    """
    return _kernel.__version__


def authors():
    """Print the authors of CymuK."""
    return 'Marc-Oliver Gewaltig <marc-oliver.gewaltig@epfl.ch>'


def SetStatus(parms):
    engine.set_status(parms)

def GetStatus(keys=None):
    """Return the parameter dictionaries of nodes or connections.

    If keys is given, a list of values is returned instead. keys may also be a
    list, in which case the returned list contains lists of values.

    Parameters
    ----------
    nodes : list or tuple
        Either a list of global ids of nodes, or a tuple of connection
        handles as returned by GetConnections()
    keys : str or list, optional
        String or a list of strings naming model properties. GetDefaults then
        returns a single value or a list of values belonging to the keys
        given.

    Returns
    -------
    dict:
        All parameters
    type:
        If keys is a string, the corrsponding default parameter is returned
    list:
        If keys is a list of strings, a list of corrsponding default parameters
        is returned

    Raises
    ------
    TypeError
        Description
    """

    return engine.get_status(keys)

def Simulate(t):
    """Simulate the network for t milliseconds.

    Parameters
    ----------
    t : float
        Time to simulate in ms
    """
    if t<0:
        raise ValueError("Simulation time must be positive")

    engine.simulate(t)


def ResetKernel():
    """Reset the simulation kernel.

    This will destroy the network as well as all custom models created with
    CopyModel(). Calling this function is equivalent to restarting NEST.
    """
    engine.init()


def ResetNetwork():
    """Reset all nodes and connections to their original state.
    """

    engine.init()


def SetKernelStatus(params):
    """Set parameters for the simulation kernel.

    Parameters
    ----------
    params : dict
        Dictionary of parameters to set.

    See also
    --------
    GetKernelStatus
    """
    engine.set_kernel_status(params)


def GetKernelStatus(keys=None):
    """Obtain parameters of the simulation kernel.

    Parameters
    ----------
    keys : str or list, optional
        Single parameter name or list of parameter names

    Returns
    -------
    dict:
        Parameter dictionary, if called without argument
    type:
        Single parameter value, if called with single parameter name
    list:
        List of parameter values, if called with list of parameter names

    Raises
    ------
    TypeError
    """

    return engine.get_kernel_status()

def Create(n=1, params=None):
    """Create n instances of type model.

    Parameters
    ----------
    model : str
        Name of the model to create
    n : int, optional
        Number of instances to create
    params : TYPE, optional
        Parameters for the new nodes. A single dictionary or a list of
        dictionaries with size n. If omitted, the model's defaults are used.

    Returns
    -------
    list:
        Global IDs of created nodes
    """

    first_gid = engine.getN_neurons()
    last_gid= engine.allocate(n)

    gids = tuple(range(first_gid, last_gid))

    if params is not None and not isinstance(params, dict):
        try:
            SetStatus(gids, params)
        except:
            raise

    return gids


def test():
    """Runs all CymuK unit tests."""
    from . import tests
    import unittest

    runner = unittest.TextTestRunner(verbosity=2)
    runner.run(tests.suite())

def Connect(pre, post, weight, delay):
    """
    Connect pre nodes to post nodes.
    """
    engine.connect(pre,post,weight, delay)

def connect_fixed_indegree(pre, n_pre, post, n_post, nc, w, d):
    """
    Randomly connect neurons with fixed number of afferent synapses.
    :param pre: gid of first pre-synaptic neuron
    :param n_pre: number of pre-synaptic neurons
    :param post: gid of first post-synaptic neuron
    :param n_post: number of post-synaptic neurons
    :param nc: number of afferent synapses per post neuron
    :param w: weight
    :param d: delay (ms >0)
    :return: no return value
    """
    engine.connect_fixed_indegree(pre, n_pre, post, n_post, nc, w, d)

def connect_fixed_outdegree(pre, n_pre, post, n_post, nc, w, d):
    """
    Randomly connect neurons with fixed number of efferent synapses.
    :param pre: gid of first pre-synaptic neuron
    :param n_pre: number of pre-synaptic neurons
    :param post: gid of first post-synaptic neuron
    :param n_post: number of post-synaptic neurons
    :param nc: number of efferent synapses per post neuron
    :param w: weight
    :param d: delay (ms >0)
    :return: no return value
    """
    engine.connect_fixed_outdegree(pre, n_pre, post, n_post, nc, w, d)

def DataConnect(pre, params=None, model="static_synapse"):
    """Connect neurons from lists of connection data.

    Parameters
    ----------
    pre : list
        Presynaptic nodes, given as lists of GIDs or lists
        of synapse status dictionaries. See below.
    params : list, optional
        See below
    model : str, optional
        Synapse model to use, see below

    Raises
    ------
    TypeError

    Usage Variants
    --------------

    Variant 1
    ~~~~~~~~~

    Connect each neuron in pre to the targets given in params,
    using synapse type model.

    - pre: [gid_1, ... gid_n]
    - params: [ {param_1}, ..., {param_n} ]
    - model= 'synapse_model'

    The dictionaries param_1 to param_n must contain at least the
    following keys:
    - 'target'
    - 'weight'
    - 'delay'
    Each key must resolve to a list or numpy.ndarray of values.

    Depending on the synapse model, other parameters can be given
    in the same format. All arrays in params must have the same
    length as 'target'.

    Variant 2
    ~~~~~~~~~

    Connect neurons according to a list of synapse status dictionaries,
    as obtained from GetStatus.

    pre = [ {synapse_state1}, ..., {synapse_state_n}]
    params=None
    model=None

    During connection, status dictionary misses will not raise errors,
    even if the kernel property 'dict_miss_is_error' is True.
    """

    if not is_sequence(pre):
        raise TypeError(
            "pre must be a list of nodes or connection dictionaries")

    if params is not None:

        if not is_sequence(params):
            raise TypeError("params must be a list of dictionaries")

        #cmd = '({0}) DataConnect_i_D_s '.format(model)

        for gid, pd in zip(pre, params):
            if 'targets' not in pd:
                raise TypeError("Parameter dict of gid={} must contain 'targets'".format(gid))
            targets= pd['targets']
            num_con=len(targets)
            if not 'delays' in pd:
                delays=np.zeros(num_con)+engine.getInt_step()
            else:
                delays=pd['delays']
            if not 'weights' in pd:
                weights=np.zeros(num_con)
            else:
                weights=pd['weights']
            engine.data_connect(gid,targets, weights, delays)

def GetConnections(pre):
    """

    :type pre: list of ints representing gids
    """
    if not is_sequence(pre):
        raise TypeError(
            "pre must be a list of nodes")
    result = [ {'targets': engine.get_targets(gid),
       'weights': engine.get_weights(gid),
       'delays': engine.get_delays(gid)} for gid in pre]
    return result

def SaveNetwork(fname):
    """
    Save the network and its dynamic state to file.
    :param fname: string with the file name
    The network is dumped in binary format and therefore platform dependent.
    """
    engine.write_file(fname)

def RestoreNetwork(fname):
    """
    Restore network state from file.
    :param fname: string with the file name
    Try to read the network state from disk. This function is optimized
    for speed and may crash if the file is corrupted.
    """
    engine.read_file(fname)