# -*- coding: utf-8 -*-
# distutils: language = c++
# distutils: sources = iaf_neurons.cpp

import cython

from libcpp.vector cimport vector
from libcpp.deque cimport deque
from libcpp.string cimport string
from libcpp cimport bool

from cpython cimport array
import sys
import numpy as np
cimport numpy as np
import types
import collections

import logging

logger = logging.getLogger('mini-me')
models = ['iaf_psc_delta']
__version__='0.0.1'

PY3 = sys.version_info[0] == 3

if PY3:
    string_types = str,
else:
    string_types = basestring,

cdef extern from "../iaf_neurons.h":
    cdef cppclass IaF_Neurons:
        IaF_Neurons()
        IaF_Neurons(IaF_Neurons)
        IaF_Neurons(double)
        size_t allocate(int)
        void init()
        void calibrate()
        void simulate(double)
        void connect(size_t, size_t, float, double d)
        void connect(size_t, vector[size_t]&, vector[float]&, vector[double]&)
        void connect_fixed_indegree(size_t, size_t, size_t, size_t, size_t, float, double)
        void connect_fixed_outdegree(size_t, size_t, size_t, size_t, size_t, float, double)
        void fill_rng_buff()

        vector[double] & getV_m()
        vector[double] & getI_syn()
        vector[size_t] & getT_spike()

        double getNu_ext()
        void setNu_ext(double)
        double getJ_ext()
        void setJ_ext(double)
        size_t get_num_spikes()
        size_t getN_neurons()
        size_t getClock()
        double getInt_step()
        double getMax_delay()
        void setInt_step(double)
        double getTau_rate()
        void setTau_rate(double)
        bool getNoise()
        void setNoise(bool)

        void setR_m(const vector[double] &)
        void setTau_m(const vector[double] &)
        void setV_m(const vector[double] &)
        void setV_rest(const vector[double] &)
        void setV_th(const vector[double] &)
        void setT_ref(const vector[double] &)
        void setI_ext(const vector[double] &)

        vector[double] & getR_m()
        vector[double] & getTau_m()
        vector[double] & getV_rest()
        vector[double] & getV_th()
        vector[double] & getT_ref()
        vector[double] & getRate()
        vector[double] & getI_ext()
        vector[size_t] & getTargets(size_t)
        vector[float] & getWeights(size_t)
        vector[unsigned char] & getDelays(size_t)

        deque[size_t]& get_spike_times()
        deque[size_t]& get_spike_ids()
        void setTargets(size_t, vector[size_t] &)
        void setWeights(size_t, vector[float] &)
        void setDelays(size_t, vector[unsigned char] &)
        void read_file(string &)
        void write_file(string &)


cdef class IaF_Network:
    """
    Network of integrate and fire neurons with delta shaped post-synaptic currents.
    """
    cdef IaF_Neurons iafn
    def __cinit__(self, double h=0.1):
        self.iafn = IaF_Neurons()

    def init(self):
        """
        Reset state of all neurons and clear all buffers.
        This function resets all neurons to their initial state, however the connections
        remain unchanged.
        :param self:
        :return:
        """
        self.iafn.init()

    def getModel(self):
        """
        Return name of the model as string.
        :param self:
        :return: name of the model in NEST convention.
        """
        return 'iaf_psc_delta'

    def allocate(self, size_t n):
        """
        Allocate space for n more neurons.
        :param n: number of neurons to be created.
        :return: ID of the last created neuron.
        """
        return self.iafn.allocate(n)


    def create(self, size_t n):
        """
        Create n more neurons.
        :param n: number of neurons to be created.
        :return: tuple with the IDs of the created neurons.
        """
        first_gid = self.iafn.getN_neurons()
        last_gid= self.iafn.allocate(n)

        gids = tuple(range(first_gid, last_gid))

        return gids

    def getN_neurons(self):
        """
        Return number of neurons in the network.
        :param self:
        :return: number of neurons as integer.
        """
        return self.iafn.getN_neurons()

    def getClock(self):
        """
        Return current network time.
        :param self:
        :return: network time in steps
        """
        return self.iafn.getClock()

    def getInt_step(self):
        return self.iafn.getInt_step()

    def get_simtime(self):
        """
        Return elapsed simulation time in ms.
        :param self:
        :return: Simulation time in milliseconds.
        """
        return self.iafn.getClock()*self.iafn.getInt_step()

    def getNu_ext(self):
        """
        Return frequency of noise population
        :param self:
        :return: Frequency of noise population in spikes/sec.
        """
        return self.iafn.getNu_ext()

    def getNoise(self):
        return self.iafn.getNoise()

    def setNoise(self, bool noise):
        self.iafn.setNoise(noise)

    def getNum_spikes(self):
        """
        Return total number of spikes produced by the network
        :param self:
        :return: Number of spikes of all neurons.
        """
        return self.iafn.get_num_spikes()

    def getSpike_times(self):
        cdef np.ndarray result=np.zeros(self.iafn.get_spike_times().size(),np.uint64)
        cdef Py_ssize_t i
        for i in xrange(result.shape[0]):
            result[i]=self.iafn.get_spike_times()[i]
        return np.asarray(result)

    def getSpike_ids(self):
        cdef np.ndarray result=np.zeros(self.iafn.get_spike_ids().size(),np.uint64)
        cdef Py_ssize_t i
        for i in xrange(result.shape[0]):
            result[i]=self.iafn.get_spike_ids()[i]
        return result

    def get_delays(self, size_t gid):
        if gid > self.getN_neurons():
            logging.error("Neuron id {} is out of range.".format(gid))
            raise IndexError
        cdef np.ndarray result=np.array(self.iafn.getDelays(gid))*self.iafn.getInt_step()
        return result

    def connect(self, size_t pre, size_t post, float w, double d):
        if(d<=0.0):
            logging.error("Delay must be positive.")
            return
        if pre > self.getN_neurons():
            logging.error("Pre-synaptic neuron is out of range.")
            raise IndexError
        if post> self.getN_neurons():
            logging.error("Post-synaptic neuron is out of range.")
            raise IndexError
        self.iafn.connect(pre, post, w, d)

    def connect_fixed_indegree(self, size_t pre, size_t n_pre,
                                    size_t post, size_t n_post,
                                    size_t nc, float w, double d):
        if d <=0:
            logging.error("Delay must be positive")
            raise ValueError
        if pre>self.getN_neurons() or pre+n_pre > self.getN_neurons():
            logging.error("Pre-synaptic neurons are out of range.")
            raise IndexError
        if post> self.getN_neurons() or post+n_post>self.getN_neurons():
            logging.error("Post-synaptic neurons are out of range.")
            raise IndexError
        self.iafn.connect_fixed_indegree(pre, n_pre, post, n_post, nc, w, d)

    def connect_fixed_outdegree(self, size_t pre, size_t n_pre,
                                    size_t post, size_t n_post,
                                    size_t nc, float w, double d):
        if d <=0:
            logging.error("Delay must be positive")
            raise ValueError
        if pre+n_pre > self.getN_neurons():
            logging.error("Pre-synaptic neurons are out of range.")
            raise IndexError
        if post+n_post>self.getN_neurons():
            logging.error("Post-synaptic neurons are out of range.")
            raise IndexError
        self.iafn.connect_fixed_outdegree(pre, n_pre, post, n_post, nc, w, d)

    def connect_one_to_many(self, size_t pre, vector[size_t] &post, vector[float] &weights, vector[double]& delays):
        if not (post.size() == weights.size() == delays.size()):
            logging.error("Dimensions of connections for neuron {} don't agree".format(pre))
            raise ValueError
        if pre > self.getN_neurons():
            logging.error("Pre-synaptic neuron is out of range.")
            raise IndexError
        cdef size_t p
        cdef size_t n_post=post.size()
        cdef size_t nneurons=self.getN_neurons()
        for p in post:
            if p > nneurons:
                logging.error("Pre-synaptic neuron id {} is out of range.".format(p))
                raise IndexError

        self.iafn.connect(pre, post, weights, delays)

    def connect_many_to_one(self, vector[size_t] &pre,  size_t post,
                                        vector[double] &weights, vector[double]& delays):
        if not(pre.size() == weights.size() == delays.size()):
            logging.error("Dimensions of connections for neuron {} don't agree.".format(post))
            raise ValueError
        if post > self.getN_neurons():
            logging.error("Post-synaptic neuron is out of range.")
            raise IndexError
        cdef size_t p
        cdef size_t n_pre=pre.size()
        cdef size_t nneurons=self.getN_neurons()
        for p in range(n_pre):
            if pre[p]>nneurons:
                logging.error("Post-synaptic neuron id {} is out of range.".format(pre[p]))
                raise IndexError

        for p in range(n_pre):
            self.iafn.connect(pre[p], post, weights[p], delays[p])


    def connect_one_to_one(self, vector[long] &pre,  vector[long] &post,
                                        vector[double] &weights, vector[double] &delays):
        if not(pre.size() == post.size() == weights.size() == delays.size()):
            logging.error("Dimensions of connections for neuron don't agree.")
            raise ValueError
        cdef size_t p
        cdef size_t n_pre=pre.size()
        cdef size_t nneurons=self.getN_neurons()
        for p in range(n_pre):
            if pre[p]>nneurons:
                logging.error("Post-synaptic neuron id {} is out of range.".format(pre[p]))
                raise IndexError
            if post[p]>nneurons:
                logging.error("Pre-synaptic neuron id {} is out of range.".format(post[p]))
                raise IndexError

        for p in range(n_pre):
            self.iafn.connect(pre[p], post[p], weights[p], delays[p])

    def simulate(self, double t):
        if(t<=self.iafn.getInt_step ()):
            logging.error("Simulation time must be positive and larger than one integration step.")
            return
        self.iafn.simulate(t)

    def calibrate(self):
        self.iafn.calibrate()

    def get_targets(self, size_t gid):
        if gid > self.getN_neurons():
            logging.error("Neuron id {} is out of range.".format(gid))
            raise IndexError

        return np.asarray(self.iafn.getTargets(gid))

    def get_weights(self, size_t gid):
        if gid > self.getN_neurons():
            logging.error("Neuron id {} is out of range.".format(gid))
            raise IndexError

        cdef size_t n= self.iafn.getWeights(gid).size()
        cdef float *ptr = &(self.iafn.getWeights(gid)[0])
        cdef float[::1]y = <float[:n]> ptr
        return np.asarray(y)


    def set_targets(self, size_t gid, vector[size_t] &targets):
        if gid > self.getN_neurons():
            logging.error("Neuron id {} is out of range.".format(gid))
            raise IndexError

        if(targets.size() != self.iafn.getTargets(gid).size()):
            logging.warning("set_targets warning: new target list has different size than current.")
        self.iafn.setTargets(gid, targets)

    def set_weights(self, size_t gid, vector[float] &weights):
        if gid > self.getN_neurons():
            logging.error("Neuron id {} is out of range.".format(gid))
            raise IndexError

        if(weights.size() != self.iafn.getWeights(gid).size()):
            logging.warning("set_weights warning: new weight list has different size than current.")
        self.iafn.setWeights(gid, weights)

    def get_kernel_status(self, key=None):
        d={}
        d['model']= self.getModel()
        d['network_size']=self.iafn.getN_neurons()
        d['resolution']= self.iafn.getInt_step()
        d['time']= self.iafn.getInt_step()*self.iafn.getClock()
        d['clock']= self.iafn.getClock()
        d['nu_ext']= self.iafn.getNu_ext()
        d['j_ext']= self.iafn.getJ_ext()
        d['n_spikes']=self.iafn.get_num_spikes()
        d['events']={'times': np.asarray(self.getSpike_times()),
                     'senders':np.asarray(self.getSpike_ids())}
        d['min_delay']=self.iafn.getInt_step()
        d['get_max_delay']= self.iafn.getMax_delay()
        d['tau_rate']=self.iafn.getTau_rate()
        d['with_noise']= self.iafn.getNoise()

        if key==None:
            return d
        elif isinstance(key, string_types):
            return d[key]
        elif isinstance(key,(tuple, list, range)):
            return {k: d[k] for k in key}

        logger.error("key must be a string or list of strings.")
        raise TypeError()


    def set_kernel_status(self, params):
        """
        Set parameters of the simulation
        :param params: Dictionary with simulation parameters.
        'resolution' integration time step of the simulation in ms.
        'nu_ext' firing rate of an imagined external noise population.
        'tau_rate' time constant in ms to estimate the firing rates of the neurons.
        'events' if this key is present, the current event recording buffers are cleared.
        """
        if 'nu_ext' in params:
            self.iafn.setNu_ext(params['nu_ext'])
        if 'j_ext' in params:
            self.iafn.setJ_ext(params['j_ext'])
        if 'resolution' in params:
            self.iafn.setInt_step(params['resolution'])
        if 'tau_rate' in params:
            self.iafn.setTau_rate(params['tau_rate'])
        if 'events' in params:
            self.iafn.get_spike_ids().clear()
            self.iafn.get_spike_times().clear()
        if 'with_noise' in params:
            self.iafn.setNoise(params['with_noise'])

    def get_status(self, key=None):
        """
        Return dictionary with network state
        :param key: Optional parameter. If set, only this property will be returned as numpy array.
        :return: dictionary if key is not given, numpy array otherwise.
        All properties, except 'model' are returned as memory views to the C++ data-structures.
        Thus, users can change the network by changing the returned arrays. Since this is a raw memory
        access, assigning incompatible values can result in a crash of the program.
        """
        d={}
        cdef size_t n= self.iafn.getN_neurons()
        cdef double *pV_m = &(self.iafn.getV_m()[0])
        cdef double[::1]V_m = <double[:n]> pV_m
        d['V_m']= np.asarray(V_m)
        cdef double *pR_m = &(self.iafn.getR_m()[0])
        cdef double[::1]R_m = <double[:n]> pR_m
        d['R_m']= np.asarray(R_m)
        cdef double *ptau_m = &(self.iafn.getTau_m()[0])
        cdef double[::1]tau_m = <double[:n]> ptau_m
        d['tau_m']= np.asarray(tau_m)
        cdef double *pV_rest = &(self.iafn.getV_rest()[0])
        cdef double[::1]V_rest = <double[:n]> pV_rest
        d['V_rest']= np.asarray(V_rest)
        cdef double *pI_e = &(self.iafn.getI_ext()[0])
        cdef double[::1]I_e = <double[:n]> pI_e
        d['I_e']= np.asarray(I_e)
        cdef double *pI_syn = &(self.iafn.getI_syn()[0])
        cdef double[::1]I_syn = <double[:n]> pI_syn
        d['I_syn']= np.asarray(I_syn)
        cdef double *prate = &(self.iafn.getRate()[0])
        cdef double[::1]rate = <double[:n]> prate
        d['rate']=np.asarray(rate)
        cdef double *pt_ref = &(self.iafn.getT_ref()[0])
        cdef double[::1]t_ref = <double[:n]> pt_ref
        d['t_ref']=np.asarray(t_ref)
        d['t_spike']=np.asarray(self.iafn.getT_spike())

        if key==None:
            return d
        elif isinstance(key, string_types):
            return d[key]
        elif isinstance(key,(tuple, list, range)):
            return {k: d[k] for k in key}

        logger.error("key must be a string or list of strings.")
        raise TypeError()


    def set_status(self, params):
        if type(params) is not dict:
            raise TypeError("params must be a valid parameter dictionary.")

        if 'I_e' in params:
            self.iafn.setI_ext(params['I_e'])
        if 'V_m' in params:
            self.iafn.setV_m(params['V_m'])
        if 'V_th' in params:
            self.iafn.setV_th(params['V_th'])
        if 'R_m' in params:
            self.iafn.setR_m(params['R_m'])
        if 't_ref' in params:
            self.iafn.setT_ref(params['t_ref'])
        if 'tau_m' in params:
            self.iafn.setTau_m('tau_m')
        if 'V_rest' in params:
            self.iafn.setV_rest('V_rest')

    def write_file(self, string fname):
        self.iafn.write_file(fname)

    def read_file(self, string fname):
        self.iafn.read_file(fname)


