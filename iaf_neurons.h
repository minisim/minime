/* 
 * File:   iaf_neurons.h
 * Author: gewaltig
 *
 * Created on September 22, 2016, 9:52 AM
 * Current version November 4, 2017
 */

#ifndef IAF_NEURONS_H
#define	IAF_NEURONS_H

#include <cassert>
#include <vector>
#include <deque>
#include <random>


class IaF_Neurons {

public:

	IaF_Neurons()= default;
	explicit IaF_Neurons(IaF_Neurons const&)= default;
	~IaF_Neurons()= default;

	/**
	 * Allocate n additional neurons.
	 * @returns new number of neurons.
	 */

	size_t allocate(size_t n_neurons);

	/**
	 * Fill or refill the buffer of Poisson random numbers.
	 */
	void fill_rng_buff();

	/**
	 * Initialize the neuron state.
	 */
	void init();

	/**
	 * Connect two neurons.
	 * @param s_gid gid of the  source neuron
	 * @param t_gid gid of the target neuron
	 * @param w weight of the connection
	 * @param d delay of the connection
	 */

	void connect(size_t s_gid, size_t t_gid, float w, double d = 1.0);

	void connect(size_t gid,
	             std::vector<size_t> const& tgts,
	             std::vector<float> const& wghts,
	             std::vector<double> const &dlys);

/**
 * Connect two popolations randomly with fixed number of afferent synapses.
 * @param pre gid of the first pre-synaptic neuron
 * @param n_pre number of pre-synaptic neuron
 * @param post gid of the fist post-synaptic neuron
 * @param n_post number of post-synaptic neurons
 * @param indegreee number of connections per post-neuron
 * @param w weight
 * @param d delay (ms)
 */
	void connect_fixed_indegree(size_t pre, size_t n_pre,
	                            size_t post, size_t n_post,
	                            size_t indegreee, float w, double d);
/**
 * Connect two popolations randomly with fixed number of efferent synapses.
 * @param pre gid of the first pre-synaptic neuron
 * @param n_pre number of pre-synaptic neuron
 * @param post gid of the fist post-synaptic neuron
 * @param n_post number of post-synaptic neurons
 * @param outdegreee number of connections per pre-neuron
 * @param w weight
 * @param d delay (ms)
	 */
	void connect_fixed_outdegree(size_t pre, size_t n_pre,
	                             size_t post, size_t n_post,
	                             size_t outegreee, float w, double d);

	/**
	 * Return reference to a neuron's target list
	 * @param s_gid global id of the source neuron
	 * @return reference to a deque containing the gids of the target neurons.
	 */
	std::vector<size_t> & getTargets(size_t s_gid)
	{
		return targets.at(s_gid);
	}

	/**
	 * Return weights of a neuron's outgoing connections
	 * @param s_gid global id of the source neuron
	 * @return reference to a vector<float> containing the weights of the connections.
	 */
	std::vector<float> & getWeights(size_t s_gid)
	{
		return weights.at(s_gid);
	}

	/**
	 * Return delays of a neuron's outgoing connections
	 * @param s_gid global id of the source neuron
	 * @return reference to a deque<double> containing the weights of the connections.
	 */
	std::vector<unsigned char> & getDelays(size_t s_gid)
	{
		return delays.at(s_gid);
	}

	/**
	 * Return external currents.
	 *
	 * @return reference to a vector with external currents (in pA)
	 */
	std::vector<double> &get_i_ext()
	{
		return i_ext;
	}


	/**
	 * Return the total number of spikes produced by the network.
	 * @return number of spike events
	 */
	size_t get_num_spikes() const;


	/**
	 * Return the list of spikes, produced by the network.
	 * @return reference to the list of spike times in
	 * multiples of the integration step.
	 */
	std::deque<size_t> & get_spike_times();

	/**
	 * Return the GIDs of neurons that have spiked.
	 * @return  reference to the deque, containing the GIDs
	 */
	std::deque<size_t> & get_spike_ids();

	/**
	 * Simulate the network for simtime milliseconds.
	 * @param simtime simulation time in ms
	 * @param init - re-calibrate before simulation
	 */
	void simulate(double simtime);

	/**
	 * Recalculate all constants of the neuron model.
	 */
	void calibrate();

	/**
	 * Return simulation time in steps
	 * @return simulation time
	 */
	size_t getClock() const;

	/**
	 * Access the membrane potentials of all neurons
	 * @return const reference to a vector<double>.
	 */
	std::vector<double> & getV_m();
	std::vector<double> & getI_syn() ;
	std::vector<size_t> &getT_spike() ;

	std::vector<double> &getTau_m() ;

	std::vector<double> &getR_m() ;

	std::vector<double> &getV_rest() ;

	std::vector<double> &getV_th() ;

	std::vector<double> &getI_ext() ;

	double getJ_ext() const;
	void setJ_ext(double j_ext);

	void setI_ext(const std::vector<double> &i_ext);

	void setV_m(const std::vector<double> &v_m);

	void setI_syn(const std::vector<double> &i_syn);

	/**
	 * Set the connection targets for neuron gid.
	 * @param gid global id of the neuron
	 * @param targets vector of unsigned long indices.
	 * The validity of this vector is not checked!
	 */
	void setTargets(size_t gid, const std::vector<size_t> &targets);

	/**
	 * Set the connection delays for neuron gid.
	 * @param gid global id of the neuron
	 * @param delay vector of unsigned char.
	 * The validity of this vector is not checked!
	 */
	void setDelays(size_t gid, const std::vector<unsigned char> &delays);

	/**
	 * Set the connection weights for neuron gid.
	 * @param gid global id of the neuron
	 * @param weight vector of doubles.
	 * The validity of this vector is not checked!
	 */
	void setWeights(size_t gid, const std::vector<float> &weights);


	size_t getN_neurons() const; //!< Return number of neurons.
	double getMax_delay();
	double getInt_step() const;
	void setInt_step(double int_step);

	void setR_m(const std::vector<double> &R_m);
	void setTau_m(const std::vector<double> &tau_m);
	void setV_rest(const std::vector<double> &v_rest);
	void setV_th(const std::vector<double> &v_th);
	void setT_ref(const std::vector<double> &t_ref);

	std::vector<double> &getT_ref();

	void setNu_ext(double nu_ext);
	double getNu_ext() const;

	void setTau_rate(double tau_rate);
	void setRate_norm(double rate_norm);
	void setRate(const std::vector<double> &rate);

	double getTau_rate() const;
	double getRate_norm() const;
	std::vector<double> &getRate();

	/**
	 * Returns true, if the network is simulated with background noise, false otherwise.
	 * @return bool
	 */
	bool getNoise() const
	{
		return noise;
	}
	/**
	 * Change noise state.
	 * Call with true and the network will be stimulated with Poisson noise.
	 * Call with false to disable the background noise.
	 */
	void setNoise(bool n)
	{
		noise=n;
	}

	void write_file(const std::string &fname) const;
	void read_file(const std::string &fname);
private:
	bool recalibrate=true;        //!< Call calibrate before simulation
	bool noise=true;              //!< Simulate with background noise
	size_t max_rng=1024;          //!< Size of the RNG buffer as power of 2
	size_t n_neurons=0;           //!< number of neurons
	size_t clock_=0;              //!< global simulation clock.

	double int_step=0.1;          //!< integration step size in ms
	double nu_ext=20.0; 	        //!< background rate
	double j_ext=0.1;             //!< Weight of the background neurons
	double tau_rate=0.050;        //!< Time constant for rate estimation in 1/s
	double rate_norm=1.0;         //!< Normalization factor for the rate
	double c_rate;                //!< constant for rate-ODE integration
	// Connectivity
	size_t max_delay_steps=0;      //!< steps per synaptic delay
	// Neuron parameters
	std::vector<double> R_m;      //!< Membrane resistance
	std::vector<double> tau_m;    //!< Membrane time constant
	std::vector<double> v_rest;   //!< Resting value of u_m
	std::vector<double> v_th;     //!< Spike threshold
	std::vector<double> t_ref;    //!< refractory time (ms)
	std::vector<double> c1;       //!< Constant for exact ODE integration
	std::vector<double> c2;       //!< Constant for exact ODE integration
	std::vector<double> i_ext;    //!< per neuron external current
	std::vector<double> v_m;      //!< Membrane potential
	std::vector<double> i_syn;    //!< Synaptic current
	std::vector<double > rate;    //!< Per neuron firing rate
 	std::vector<size_t> t_spike;  //!< time of last spike
	std::vector<unsigned char > rng_buff; //!< Buffer for Poisson numbers
	std::vector<unsigned char> refractory_steps; //!< steps per t_ref
  std::vector<std::vector<size_t> > targets; //!< list of targets
	std::vector<std::vector<unsigned char> > delays; //!< list of delays
	std::vector<std::vector<float> > weights; //!< list of weights
  std::vector<std::vector<float> > queue; //!< Input buffer for spikes
	std::deque<size_t> spike_list; //!< list of all spikes in order
	std::deque<size_t> id_list;    //!< list of neurons for spike_list in order

  /**
   *  Update the state of the neurons by n_steps simulation steps.
   */
  void update(size_t n_steps);

	/**
	 * Re-calculate the maximal delay and scale the buffers.
	 * Normally this function is called, by calibrate if the connections have changed.
	 * The complexity of this function is proportional to the number of connections.
	 */
	void setMax_delay();

	/**
	 * Convert delays from ms to simulation steps.
	 * @param delay in ms
	 * @return the smallest number of integration steps >= delay
	 */
	unsigned char delay_to_steps(double delay) const;

	/**
	 * Convert time from ms to simulation steps.
	 * @param delay in ms
	 * @return the smallest number of integration steps >= delay
	 */
	size_t time_to_steps(double) const;

};

#endif	/* IAF_NEURONS_H */

