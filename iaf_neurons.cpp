#ifdef NDEBUG
#undef NDEBUG
#endif

#include <cassert>
#include <iostream>
#include <cmath>
#include <random>
#include <fstream>
#include <algorithm>

#include "iaf_neurons.h"
#include "xorshift.h"

size_t IaF_Neurons::allocate(size_t n) {
	n_neurons += n;

	tau_m.resize(n_neurons, 20.0);
	R_m.resize(n_neurons, 20.0);
	refractory_steps.resize(n_neurons, 0);
	v_th.resize(n_neurons, 20.0);
	v_rest.resize(n_neurons, 0.0);
	t_ref.resize(n_neurons, 2.0);
	rate.resize(n_neurons, 0.0);

	v_m.resize(n_neurons, 0.0);
	c1.resize(n_neurons);
	c2.resize(n_neurons);
	i_syn.resize(n_neurons, 0.0);
	i_ext.resize(n_neurons, 0.0);
	t_spike.resize(n_neurons, 0);

	targets.resize(n_neurons);
	delays.resize(n_neurons);
	weights.resize(n_neurons);
	rng_buff.resize(max_rng, 0);


	max_delay_steps = 0;
	recalibrate=true;
	return n_neurons;
}

void IaF_Neurons::init() {
	std::fill(i_syn.begin(), i_syn.end(),0.0);
	std::fill(i_ext.begin(), i_ext.end(),0.0);
	std::fill(rate.begin(), rate.end(),0.0);
	std::fill(t_spike.begin(),t_spike.end(),0);

	for(size_t n=0; n<n_neurons;++n) {
		v_m[n] = v_rest[n];
	}

	for (size_t t = 0; t < queue.size(); ++t) {
		std::fill(queue[t].begin(), queue[t].end(), 0.0);
	}
	spike_list.clear();
	id_list.clear();
}

void IaF_Neurons::setMax_delay() {
	unsigned char maxd = 0;
	for (auto delst: delays) {
		for (auto d: delst) {
			maxd = std::max(d, maxd);
		}
	}
	// We make max_delay_steps a power of 2 for faster modulo.
	max_delay_steps = 1UL << static_cast<size_t>(std::floor(std::log2(maxd)) + 1);
	queue.resize(max_delay_steps);
	for (size_t t = 0; t < queue.size(); ++t) {
		queue[t].resize(n_neurons, 0.0);
	}
}

void IaF_Neurons::calibrate() {
	if (max_delay_steps == 0) {
		setMax_delay();
	}
	// Constants for rate estimation.
	// rate_norm is such that the exponential filter has area 1.0
	rate_norm = 1.0 / tau_rate;
	c_rate = std::exp(-int_step / 1000. * rate_norm); // in 1/s

	for (size_t gid = 0; gid < n_neurons; ++gid) {
		c1[gid] = std::exp(-int_step / tau_m[gid]);
		c2[gid] = R_m[gid] * (1.0 - c1[gid]);
		refractory_steps[gid] = delay_to_steps(t_ref[gid]);
	}
	if (noise) {
		fill_rng_buff();
	}
	recalibrate=false;
}

void IaF_Neurons::fill_rng_buff() {
	static std::random_device rd;
	static std::mt19937 gen(rd());
	std::poisson_distribution<unsigned char> bg_spikes(nu_ext * int_step);

	for (auto &r: rng_buff) {
		r = bg_spikes(gen);
	}
}


void IaF_Neurons::update(size_t n_steps) {
	static std::vector<size_t> spikes(n_neurons);
	const auto t_stop = clock_ + n_steps;
	const auto mds = max_delay_steps - 1;
	const auto mrd = max_rng - 1;


	while (clock_ < t_stop) {
		size_t n_spikes = 0;


		for (size_t i = 0; i < n_neurons; ++i) {
			spikes[n_spikes] = i;
			n_spikes += (v_m[i] > v_th[i]);
		}

		for (size_t i = 0; i < n_spikes; ++i) {
			rate[spikes[i]] += rate_norm;
		}
		for (size_t i = 0; i < n_neurons; ++i) {
			rate[i] *= c_rate;
		}

		for (size_t i = 0; i < n_spikes; ++i) {
			t_spike[spikes[i]] = clock_;
		}

		for (size_t i = 0; i < n_spikes; ++i) {
			for (size_t j = 0; j < targets[spikes[i]].size(); ++j) {
				// We use a bitwise & for % as the max_delay is power of 2.
				queue[(clock_ + delays[spikes[i]][j]) & mds][targets[spikes[i]][j]] += weights[spikes[i]][j];
			}
		}

		if(noise == true) {
			for (size_t i = 0; i < n_neurons; ++i) {
				i_syn[i] = queue[clock_ & mds][i] + j_ext * rng_buff[xor128() & mrd];
			}
		}
		else {
			for (size_t i = 0; i < n_neurons; ++i) {
				i_syn[i] = queue[clock_ & mds][i];
			}
		}

		for (size_t i = 0; i < n_neurons; ++i) {
			v_m[i] = c1[i] * v_m[i] + c2[i] * i_ext[i] + i_syn[i];
		}

		for (size_t i = 0; i < n_neurons; ++i) {
			if ((t_spike[i] > 0) and (clock_ - t_spike[i] < refractory_steps[i]))
				v_m[i] = v_rest[i];
		}

		for (size_t i = 0; i < n_neurons; ++i) {
			queue[clock_ & mds][i] = 0.0;
		}

		for (size_t i = 0; i < n_spikes; ++i) {
			spike_list.push_back(clock_);
			id_list.push_back(spikes[i]);
		}

		++clock_;
	}
}

void IaF_Neurons::simulate(double simtime) {
	if( recalibrate==true)
		calibrate();

	const auto simsteps = time_to_steps(simtime);
	assert(simsteps >= max_delay_steps);
	update(simsteps);
}

void IaF_Neurons::connect(size_t s_gid, size_t t_gid, float w, double d) {
	max_delay_steps = 0; // this forces re-search of max-delay.
	assert(s_gid < weights.size());
	assert(t_gid < n_neurons);
	assert(d > 0.0);
	weights[s_gid].push_back(w);
	targets[s_gid].push_back(t_gid);
	delays[s_gid].push_back(delay_to_steps(d));
	recalibrate=true;
}

size_t IaF_Neurons::get_num_spikes() const {
	return spike_list.size();
}

std::deque<size_t> &IaF_Neurons::get_spike_ids() {
	return id_list;
}

std::deque<size_t> &IaF_Neurons::get_spike_times() {
	return spike_list;
}

void IaF_Neurons::setR_m(const std::vector<double> &R_m) {
	IaF_Neurons::R_m = R_m;
	recalibrate=false;
}

void IaF_Neurons::setTau_m(const std::vector<double> &tau_m) {
	IaF_Neurons::tau_m = tau_m;
	recalibrate=false;
}

void IaF_Neurons::setV_rest(const std::vector<double> &v_rest) {
	IaF_Neurons::v_rest = v_rest;
}

void IaF_Neurons::setV_th(const std::vector<double> &v_th) {
	IaF_Neurons::v_th = v_th;
}

void IaF_Neurons::setT_ref(const std::vector<double> &t_ref) {
	IaF_Neurons::t_ref = t_ref;
}

std::vector<double> & IaF_Neurons::getV_m()  {
	return v_m;
}

std::vector<double> & IaF_Neurons::getI_syn() {
	return i_syn;
}

 std::vector<size_t> &IaF_Neurons::getT_spike()  {
	return t_spike;
}

inline
size_t IaF_Neurons::time_to_steps(double t) const {
	return static_cast<size_t>(std::ceil(t / int_step));
}

inline
unsigned char IaF_Neurons::delay_to_steps(double d) const {
	return static_cast<unsigned char>(std::ceil(d / int_step));
}

double IaF_Neurons::getNu_ext() const {
	return nu_ext;
}

void IaF_Neurons::setNu_ext(double nu_ext) {
	IaF_Neurons::nu_ext = nu_ext;
	recalibrate=true;
}

void IaF_Neurons::setTargets(size_t gid, const std::vector<size_t> &targets) {
	IaF_Neurons::targets.at(gid) = targets;
}

void IaF_Neurons::setDelays(size_t gid, const std::vector<unsigned char> &delays) {
	max_delay_steps = 0;
	IaF_Neurons::delays.at(gid) = delays;
	recalibrate=true;
}

void IaF_Neurons::setWeights(size_t gid, const std::vector<float> &weights) {
	IaF_Neurons::weights.at(gid) = weights;
}

size_t IaF_Neurons::getN_neurons() const {
	return n_neurons;
}

std::vector<double> &IaF_Neurons::getRate()  {
	return rate;
}

void IaF_Neurons::setRate(const std::vector<double> &rate) {
	IaF_Neurons::rate = rate;
}

double IaF_Neurons::getTau_rate() const {
	return tau_rate;
}

void IaF_Neurons::setTau_rate(double tau_rate) {
	assert(tau_rate > 0.0);
	IaF_Neurons::tau_rate = tau_rate;
	recalibrate=true;
}

double IaF_Neurons::getRate_norm() const {
	return rate_norm;
}

void IaF_Neurons::setRate_norm(double rate_norm) {
	IaF_Neurons::rate_norm = rate_norm;
	recalibrate=true;
}

void IaF_Neurons::connect(size_t gid,
                          std::vector<size_t> const &trgts,
                          std::vector<float> const &wghts,
                          std::vector<double> const &dlys)
{
	const auto targetsize = trgts.size();
	const auto weightsize = wghts.size();
	const auto delaysize = dlys.size();
	assert(targetsize == weightsize);
	assert(targetsize == delaysize);
	assert(gid < targets.size());

	targets[gid].insert(targets[gid].end(), trgts.begin(), trgts.end());
	weights[gid].insert(weights[gid].end(), wghts.begin(), wghts.end());

	std::vector<unsigned char> uc_delays(dlys.size());
	const auto num_delays = delays[gid].size();
	delays[gid].insert(delays[gid].end(), delaysize, static_cast<unsigned char>(0));

	for (size_t i = 0; i < delaysize; ++i)
		delays[gid][num_delays + i] = delay_to_steps(dlys[i]);
	max_delay_steps = 0;
	recalibrate=true;
}

size_t IaF_Neurons::getClock() const {
	return clock_;
}

double IaF_Neurons::getInt_step() const {
	return int_step;
}

double IaF_Neurons::getMax_delay() {
	setMax_delay();
	return int_step * max_delay_steps;
}

void IaF_Neurons::setInt_step(double int_step) {
	IaF_Neurons::int_step = int_step;
	max_delay_steps = 0; // force buffer reallocation
	recalibrate=true;
}

std::vector<double> &IaF_Neurons::getTau_m()  {
	return tau_m;
}

std::vector<double> &IaF_Neurons::getR_m()  {
	return R_m;
}

std::vector<double> &IaF_Neurons::getV_rest()  {
	return v_rest;
}

std::vector<double> &IaF_Neurons::getV_th()  {
	return v_th;
}

std::vector<double> &IaF_Neurons::getI_ext()  {
	return i_ext;
}

double IaF_Neurons::getJ_ext()  const {
	return j_ext;
}

void IaF_Neurons::setJ_ext(double j_ext) {
	IaF_Neurons::j_ext = j_ext;
}

void IaF_Neurons::setI_ext(const std::vector<double> &i_ext) {
	IaF_Neurons::i_ext = i_ext;
}

void IaF_Neurons::setV_m(const std::vector<double> &v_m) {
	IaF_Neurons::v_m = v_m;
}

void IaF_Neurons::setI_syn(const std::vector<double> &i_syn) {
	IaF_Neurons::i_syn = i_syn;
}

std::vector<double> &IaF_Neurons::getT_ref()  {
	return t_ref;
}

void IaF_Neurons::connect_fixed_indegree(size_t pre, size_t n_pre,
                                         size_t post, size_t n_post,
                                         size_t indegreee, float w, double d) {

	const auto last_post = post + n_post;

	for (size_t target = post; target < last_post; ++target) {
		for (size_t i = 0; i < indegreee; ++i) {
			auto source = pre + xor128() % n_pre;
			connect(source, target, w, d);
		}
	}
}

void IaF_Neurons::connect_fixed_outdegree(size_t pre, size_t n_pre,
                                          size_t post, size_t n_post,
                                          size_t outdegreee, float w, double d) {
	const auto last_pre = pre + n_pre;

	for (size_t source = pre; source < last_pre; ++source) {
		for (size_t i = 0; i < outdegreee; ++i) {
			auto target = post + xor128() % n_post;
			connect(source, target, w, d);
		}
	}
}

template<class T>
void write_pod(std::ofstream &out, T val) {
	out.write(reinterpret_cast<char const *> (&val), sizeof(T));
}

template<class T>
void write_vector(std::ofstream &out, std::vector<T> const &vec) {
	size_t size = vec.size();
	write_pod(out, size);
	out.write(reinterpret_cast<const char *>(vec.data()), size * sizeof(T));
}

template<class T>
void write_vector_vector(std::ofstream &out, std::vector<T> const &vec) {
	write_pod(out, vec.size());
	for (auto &v: vec) {
		write_vector(out, v);
	}
}


void IaF_Neurons::write_file(const std::string &fname) const {
	auto out = std::ofstream(fname, std::ofstream::binary);
	write_pod(out, int_step);          //!< integration step size in ms
	write_pod(out, max_rng);          //!< Size of the RNG buffer as power of 2

	write_pod(out, n_neurons);             //!< number of neurons
	write_pod(out, clock_);                //!< global simulation clock.

	write_pod(out, nu_ext);          //!< background rate
	write_pod(out, j_ext);             //!< Weight of the background neurons
	write_pod(out, tau_rate);        //!< Time constant for rate estimation in 1/s
	write_pod(out, rate_norm);         //!< Normalization factor for the rate
	write_pod(out, c_rate);                //!< constant for rate-ODE integration
	// Connectivity
	write_pod(out, max_delay_steps);      //!< steps per synaptic delay

	//out.write(reinterpret_cast<const char *>(this), sizeof(IaF_Neurons));
	write_vector(out, R_m);      //!< Membrane resistance
	write_vector(out, tau_m);    //!< Membrane time constant
	write_vector(out, v_rest);   //!< Resting value of u_m
	write_vector(out, v_th);     //!< Spike threshold
	write_vector(out, t_ref);    //!< refractory time (ms)
	write_vector(out, c1);       //!< Constant for exact ODE integration
	write_vector(out, c2);       //!< Constant for exact ODE integration
	write_vector(out, i_ext);    //!< per neuron external current
	write_vector(out, v_m);      //!< Membrane potential
	write_vector(out, i_syn);    //!< Synaptic current
	write_vector(out, rate);    //!< Per neuron firing rate
	write_vector(out, t_spike);  //!< time of last spike
	write_vector(out, rng_buff); //!< Buffer for Poisson numbers
	write_vector(out, refractory_steps); //!< steps per t_ref
	write_vector_vector(out, targets); //!< list of targets
	write_vector_vector(out, delays); //!< list of delays
	write_vector_vector(out, weights); //!< list of weights
	write_vector_vector(out, queue); //!< Input buffer for spikes
}

template<class T>
void read_pod(std::ifstream &in, T &val) {
	in.read(reinterpret_cast<char *>(&val), sizeof(T));
}

template<class T>
void read_vector(std::ifstream &in, std::vector<T> &vec) {
	size_t size = 0;
	vec.clear();
	read_pod(in, size);
	vec.resize(size);
	in.read(reinterpret_cast<char *>(&vec[0]), vec.size() * sizeof(T));
}

template<class T>
void read_vector_vector(std::ifstream &in, std::vector<T> &vec) {
	size_t size = 0;
	read_pod(in, size);
	vec.clear();
	vec.resize(size);
	for (auto &v: vec) {
		read_vector(in, v);
	}
}


void IaF_Neurons::read_file(const std::string &fname) {
	auto in = std::ifstream(fname, std::ifstream::binary);
	read_pod(in, int_step);          //!< integration step size in ms
	read_pod(in, max_rng);          //!< Size of the RNG buffer as power of 2

	read_pod(in, n_neurons);             //!< number of neurons
	read_pod(in, clock_);                //!< global simulation clock.

	read_pod(in, nu_ext);          //!< background rate
	read_pod(in, j_ext);             //!< Weight of the background neurons
	read_pod(in, tau_rate);        //!< Time constant for rate estimation in 1/s
	read_pod(in, rate_norm);         //!< Normalization factor for the rate
	read_pod(in, c_rate);                //!< constant for rate-ODE integration
	// Connectivity
	read_pod(in, max_delay_steps);      //!< steps per synaptic delay

	read_vector(in, R_m);      //!< Membrane resistance
	read_vector(in, tau_m);    //!< Membrane time constant
	read_vector(in, v_rest);   //!< Resting value of u_m
	read_vector(in, v_th);     //!< Spike threshold
	read_vector(in, t_ref);    //!< refractory time (ms)
	read_vector(in, c1);       //!< Constant for exact ODE integration
	read_vector(in, c2);       //!< Constant for exact ODE integration
	read_vector(in, i_ext);    //!< per neuron external current
	read_vector(in, v_m);      //!< Membrane potential
	read_vector(in, i_syn);    //!< Synaptic current
	read_vector(in, rate);     //!< Per neuron firing rate
	read_vector(in, t_spike);  //!< time of last spike
	read_vector(in, rng_buff);          //!< Buffer for Poisson numbers
	read_vector(in, refractory_steps);  //!< steps per t_ref
	read_vector_vector(in, targets);    //!< list of targets
	read_vector_vector(in, delays);     //!< list of delays
	read_vector_vector(in, weights);    //!< list of weights
	read_vector_vector(in, queue); //!< Input buffer for spikes
	recalibrate=true;
}
