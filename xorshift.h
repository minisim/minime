//
// Created by gewaltig on 03.11.17.
//

#ifndef NETSIM_XORSHIFT_H
#define NETSIM_XORSHIFT_H

#include <cstdint>

/**
 * Fast random number generator.
 * https://codingforspeed.com/using-faster-psudo-random-generator-xorshift/
 * @return random unsigned integer
 */
inline
uint32_t xor128()
{
	static uint32_t x = 123456789;
	static uint32_t y = 362436069;
	static uint32_t z = 521288629;
	static uint32_t w = 88675123;
	uint32_t t;
	t = x ^ (x << 11);
	x = y; y = z; z = w;
	return w = w ^ (w >> 19) ^ (t ^ (t >> 8));
}

#endif //NETSIM_XORSHIFT_H
