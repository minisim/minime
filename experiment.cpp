 #include <cassert>
#include <iostream>
#include <fstream>
#include <cmath>
#include <random>
#include "experiment.h"



Experiment::Experiment()
  :
  net()
{
}

void Experiment::connect()
{

	net.allocate(n_neurons);
	net.setNu_ext(2.*20.0/(0.1*20.));

	net.connect_fixed_indegree(0,10000,0,12500,ce, w, 1.5);
	net.connect_fixed_indegree(10000, 2500,0,12500, ci, -w*g, 1.5);

	net.calibrate();
	std::cout << "Network initialized\n";

}

void Experiment::run(double simtime)
{
	net.simulate(simtime);
  size_t n_spikes=net.get_num_spikes();
	auto rate=n_spikes/(n_neurons*simtime/1000.);
  std::cerr << "Rate = "<< rate << '\n';
}

void Experiment::save(std::string filename)
{
  std::ofstream out(filename);
  auto &spike_list=net.get_spike_times();
  auto &id_list=net.get_spike_ids();
  assert(spike_list.size()==id_list.size());
  
  for(size_t i=0; i<spike_list.size(); ++i)
  {
    out << spike_list[i] << '\t' << id_list[i] << '\n';
  }
  
}