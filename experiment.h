#ifndef EXPERIMENT_H
#define EXPERIMENT_H
#include <iostream>
#include "iaf_neurons.h"


struct Experiment
{
  const size_t n_neurons=12500;
  const size_t ce= 1000;
  const size_t ci= 250;
  const double g=5.0;
  const double w=0.1;
  const size_t n_stim=0;

  IaF_Neurons net;

public:
  Experiment();
  void connect();
  void run(double);
  void save(std::string);

};

#endif
