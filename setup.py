# -*- coding: utf-8 -*-

import os

# the following overrides some default compiler flags that prevent certain optimizations.
#
os.environ["CFLAGS"] = "-g -O3"
from setuptools import setup, find_packages, Extension
from Cython.Build import cythonize
import numpy as np
import sysconfig

extensions = [
    Extension(
        'minisim.minikernel',
        ['minisim/minikernel.pyx']
        + ['iaf_neurons.cpp'],
        include_dirs = [np.get_include()],
        extra_compile_args=["-std=c++11"])
]

setup(
    name='minisim',
    description='A minimal high-speed simulator for integrate and fire neuron models',
    author='Marc-Oliver Gewaltig',
    version='0.1.0',
    packages=['minisim'],
    include_dirs = [np.get_include()],
    ext_modules=cythonize(extensions))
